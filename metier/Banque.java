package metier;
import java.util.*;

public class Banque {
    private String nom = "";
    private String adresse = "";
    private Set<Personne> clients;

    public Banque(String nom) {
        this.nom = nom;
        clients = new HashSet<>();
    }
   
    @Override
    public String toString() {
        return nom + " " + adresse;
    }
    
    // Accesseurs (getters/setters)
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public void ajouteClient(Personne personne) {
        clients.add(personne);
    }
}