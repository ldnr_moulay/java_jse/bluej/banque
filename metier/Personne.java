package metier;
import java.util.*;

public class Personne {
    private String nom;
    private Set<CompteBancaire> comptes;

    public Personne(String nom) {
        this.nom = nom;
        comptes = new HashSet<>();
    }

    @Override
    public String toString() {
        return nom;
    }
   
    // Accesseurs (getters/setters)

    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public void ajouteCompte(CompteBancaire cpt) {
        comptes.add(cpt);
        if (cpt.getDetenteur() != this)
            cpt.setDetenteur(this);
    }
    
    public boolean detientCompte(CompteBancaire cpt) {
        return comptes.contains(cpt);
    }
}