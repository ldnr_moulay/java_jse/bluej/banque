package metier;
import java.util.*;

/**
 * 
 */
public class CompteEpargne extends CompteBancaire {
    
    public double tauxInteret;
    
    public CompteEpargne(String numeroCompte, double solde,
        Banque banque, Personne detenteur, double tauxInteret) {
        
        super(numeroCompte, solde, banque, detenteur);
        this.tauxInteret = tauxInteret;
    }

    @Override
    public String toString() {
        return super.toString() + ", taux = " + tauxInteret;
    }
}