import metier.*;
public class Demo
{
    
    public static void start()
    {
        
        // Créer banque, personne, compte associé aux deux
        Banque bnp = new Banque("BNP");
        System.out.println(bnp);
                
        Personne paulo = new Personne("Paulo");
        System.out.println(paulo);
               
        bnp.ajouteClient(paulo);
              
        CompteBancaire cptChq = new CompteCheque("0001", 100., bnp, paulo);
        System.out.println(cptChq);

        CompteBancaire cptEp = new CompteEpargne("0002", 100., bnp, paulo, 0.75);
        System.out.println(cptEp);
        System.out.println(cptEp.getDetenteur());
    }
}
